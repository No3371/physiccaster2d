# PhysicCaster2D
This is a set of Monobehaviour that encapsulate Physic casting calls. While the configuration can be adjust on inspector and be saved with prefabs, Cast() Must be call manually by other scripts.

![Img](https://i.imgur.com/bl5qiyg.png)
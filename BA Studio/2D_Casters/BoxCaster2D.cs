﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace BA_Studio.UnityLib.Caster2D
{

	public class BoxCaster2D : PhysicsCaster2DV2
	{

		[HideInInspector]
		public Vector2 castSize;

		[RangeAttribute(-180, 180)]
		public float boxAngle;

		Collider2D[] casted;

		List<Collider2D> cache;

		public override void Start () {
			base.Start();
		}

		protected override void DoCast () {
			PreCast();

			casted = new Collider2D[maxCandidate];
			
			if (cache == null) cache = new List<Collider2D>();
			else cache.Clear();

			if (Physics2D.OverlapBox(baseTransform.position + (Vector3) CastBaseOffsetFlipped, castSize, boxAngle, filter2D, casted) == 0) return;
			// if (filter2D.useDepth) Physics2D.OverlapBoxNonAlloc(baseTransform.position + (Vector3) castBaseOffsetFlipped, castSize, boxAngle, casted, filter2D.layerMask, filter2D.minDepth, filter2D.maxDepth);
			// else Physics2D.OverlapBoxNonAlloc(baseTransform.position + (Vector3) castBaseOffsetFlipped, castSize, boxAngle, casted, filter2D.layerMask);

			// if (casted.All(e => e == null)) return;

			cache.AddRange(casted);

			cache.RemoveAll(e => e == null);
			cache.RemoveAll(e => blackList.Contains(e));
			if (whiteListMode) {
				cache.RemoveAll(e => !targetTags.Contains(e.tag));
			}
			else cache.RemoveAll(e => ignoreTags.Contains(e.tag));	
		}

        public override bool Check<T>()
        {
			DoCast();
			if (cache.Count == 0) return false;
			else return true;
        }

		public override T[] CastAll<T> (){
			DoCast();
			cache.RemoveAll(e => e.GetComponent<T>() == null);
			if (cache.Count == 0) return null;
			return cache.SelectMany(e => e.GetComponents<T>()).ToArray();
		}

		public override T Cast<T> (){
			DoCast();
			cache.RemoveAll(e => e.GetComponent<T>() == null);
			if (cache.Count == 0) return null;
			return FindClosest(cache).GetComponent<T>();
		}
		
		public override Collider2D Cast () {
			return Cast<Collider2D>();
		}

		Collider2D FindClosest (List<Collider2D> list) {
			Collider2D got = null;
			float closestDis = 0;
			
			foreach (Collider2D col2 in list) {
				float dis = (baseTransform.position - col2.transform.position).sqrMagnitude;
				if (got == null || closestDis > dis) {
					got = col2;
					closestDis = dis;
				}
			}
			return got;
		}
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BA_Studio.UnityLib.General;

namespace BA_Studio.UnityLib.Caster2D
{
	[CustomEditor(typeof(RayCaster2D))]
	public class RayCaster2DInspector : PhysicsCaster2DV2Inspector {

		protected SerializedProperty CastDirProp, CastPointsProp;

		private RayCaster2D Target;

		GUIContent dirLabel = new GUIContent("Cast Direction"), pointsLabel = new GUIContent("Cast Points");

		public void OnEnable () {
			base.OnEnable();
			CastDirProp = serializedObject.FindProperty("castDirection");
			CastPointsProp = serializedObject.FindProperty("castOffsets");
		}

		public override void OnInspectorGUI()
		{
			Target = (RayCaster2D) target;
			GUILayout.Label("Caster Settings", EditorStyles.boldLabel);
			DrawCommonFields(Target);
			GUILayout.Space(8);
			GUILayout.Label("Ray Settings", EditorStyles.boldLabel);
			GUILayout.BeginVertical();
				EditorGUILayout.PropertyField(CastDirProp, dirLabel, true);	
				if (Target.castDirection == BasicDirections.Custom) Target.castDirecitonOverride = EditorGUILayout.Vector2Field("Override Direction", Target.castDirecitonOverride);
				Target.responsiveCastRange = EditorGUILayout.Toggle("ResponsiveCastRange", Target.responsiveCastRange);
				if (Target.responsiveCastRange) {
					GUILayout.BeginHorizontal();
						EditorGUILayout.PrefixLabel("Range");
						GUILayout.Label("Min", GUILayout.Width(30));
						Target.rangeMin = EditorGUILayout.FloatField(Target.rangeMin, GUILayout.Width(40));
						GUILayout.Space(4);
						GUILayout.Label("Max", GUILayout.Width(30));
						Target.rangeMax = EditorGUILayout.FloatField(Target.rangeMax, GUILayout.Width(40));
					GUILayout.EndHorizontal();
				}
				else Target.castRange = EditorGUILayout.FloatField("Cast Range", Target.castRange);
				Target.rotateWithTransform = EditorGUILayout.Toggle("Rotate With Transform", Target.rotateWithTransform);
				EditorGUILayout.PropertyField(CastPointsProp, pointsLabel, true);	
			GUILayout.EndVertical();        

			serializedObject.ApplyModifiedProperties();
			GUILayout.Label("Debug", EditorStyles.boldLabel);
			GUILayout.BeginHorizontal();				
			if (GUILayout.Button("Cast")) Debug.Log("RayCaster2D:: " + Target.Check());
			if (GUILayout.Button("Offset?"))
				Debug.Log("RayCaster2D:: BaseCastOffset: " + Target.castBaseOffset
				+ "/n BaseCastOffsetFlipped: " + Target.CastBaseOffsetFlipped
				+ "/n P[0]: " + Target.CastPointCoords[0]);
			GUILayout.EndHorizontal();

		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BA_Studio.UnityLib.Caster2D
{
	[CustomEditor(typeof(PhysicsCaster2DV2))]
	public class PhysicsCaster2DV2Inspector : Editor {

		protected SerializedProperty TargetTagsListProp, BlackTagsListProp, BlackColListProp, filterProp;
		protected Vector2 offsetPos;

		public void OnEnable () {
			TargetTagsListProp = serializedObject.FindProperty("targetTags");
			BlackTagsListProp = serializedObject.FindProperty("ignoreTags");
			BlackColListProp = serializedObject.FindProperty("blackList");
			filterProp = serializedObject.FindProperty("filter2D");
		}

		protected void DrawCommonFields (PhysicsCaster2DV2 Target = null) {
			if (Target == null) Target = (PhysicsCaster2DV2) target;

			Undo.RecordObject(Target, "PhysicCasterSettings");
			GUILayout.BeginVertical();
				Target.ID = EditorGUILayout.TextField("ID", Target.ID);
				Target.maxCandidate = EditorGUILayout.IntField("Max Candidates", Target.maxCandidate);

				Target.castBaseOffset = EditorGUILayout.Vector2Field("Offset", Target.castBaseOffset);
				EditorGUI.indentLevel++;
				GUILayout.BeginVertical("Box");	
				EditorGUILayout.PropertyField(filterProp, true);		
				GUILayout.EndVertical();
				EditorGUI.indentLevel--;
				GUILayout.BeginVertical("Box");
					Target.whiteListMode = EditorGUILayout.Toggle("Ignore non-targets (Tags)", Target.whiteListMode);
					GUILayout.BeginHorizontal();				
						GUILayout.Space(14);
						if (Target.whiteListMode) EditorGUILayout.PropertyField(TargetTagsListProp, true);
						else EditorGUILayout.PropertyField(BlackTagsListProp, true);			
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();				
						GUILayout.Space(14);
						EditorGUILayout.PropertyField(BlackColListProp, true);					
					GUILayout.EndHorizontal();				
				GUILayout.EndVertical();
				Target.ToggleVisualization = EditorGUILayout.Toggle("Toggle Visual", Target.ToggleVisualization);
				Target.baseTransform = EditorGUILayout.ObjectField("Base Transform", Target.baseTransform, typeof(Transform), true) as Transform;
				Target.flipX = EditorGUILayout.Toggle("Flip X", Target.flipX);
				Target.flipY = EditorGUILayout.Toggle("Flip Y", Target.flipY);
			GUILayout.EndVertical();        
			serializedObject.ApplyModifiedProperties();

		}

		public override void OnInspectorGUI()
		{
			DrawCommonFields();
		}

	}

}
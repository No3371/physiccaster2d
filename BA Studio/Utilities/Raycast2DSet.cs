using UnityEngine;
using BA_Studio.UnityLib.General;
using System.Collections.Generic;
using System.Linq;


namespace BA_Studio.UnityLib.Utilities
{
    [System.Serializable]
	public class Raycast2DSet
	{
		public BasicDirections direction;

		public Vector2 customDirection;
		public Vector2 offset;
		public float castDistance;
		public ContactFilter2D filter;

		public GameObjectFilter goFilter;
		public RaycastHit2D[] previousHits;

		int maxCandidate = 99;
		public Vector2 DirectionValue
		{
			get
			{
				if (direction == BasicDirections.Custom) return customDirection;
				else return CommonLibUtil.DirectionToVector(direction);
			}
		}


        public Raycast2DSet (BasicDirections dir, Vector2 offset, float castDistance, ContactFilter2D filter = default(ContactFilter2D), GameObjectFilter gofilter = null)
        {
			this.direction = dir;
			this.offset = offset;
			this.castDistance = castDistance;
			this.filter = filter;
			this.goFilter = gofilter;
        }
		
        public Raycast2DSet (BasicDirections dir, float castDistance, ContactFilter2D filter = default(ContactFilter2D), GameObjectFilter gofilter = null)
        {
			this.direction = dir;
			this.castDistance = castDistance;
			this.filter = filter;
			this.goFilter = gofilter;
        }

        public Raycast2DSet (Vector2 dir, Vector2 offset, float castDistance, ContactFilter2D filter = default(ContactFilter2D), GameObjectFilter gofilter = null)
        {
			this.direction = BasicDirections.Custom;
			this.customDirection = dir;
			this.offset = offset;
			this.castDistance = castDistance;
			this.filter = filter;
			this.goFilter = gofilter;
        }

        public Raycast2DSet (Vector2 dir, float castDistance, ContactFilter2D filter = default(ContactFilter2D), GameObjectFilter gofilter = null)
        {
			this.direction = BasicDirections.Custom;
			this.customDirection = dir;
			this.castDistance = castDistance;
			this.filter = filter;
			this.goFilter = gofilter;
        }

        // public void CastAllNonAlloc (Vector2 origin, ref RaycastHit2D[] result)
		// {
		// 	if (filter.useDepth)
		// 		Physics2D.RaycastNonAlloc(
		// 			origin + offset,
		// 			DirectionValue,
		// 			result,
		// 			castDistance,
		// 			(filter.useLayerMask)? filter.Value.layerMask : (LayerMask) System.Int32.MaxValue,
		// 			filter.Value.minDepth,
		// 			filter.Value.maxDepth);
		// 	else Physics2D.RaycastNonAlloc(
		// 			origin + offset,
		// 			DirectionValue,
		// 			result,
		// 			castDistance,
		// 			(filter.Value.useLayerMask)? filter.Value.layerMask : (LayerMask) System.Int32.MaxValue);
		// }

		List<RaycastHit2D> cache = new List<RaycastHit2D>();
		int raycastCount;
		protected ICollection<T> CastAll<T> (Vector2 origin, Vector2 directionOverride, float distanceOverride, LayerMask layerMaskOverride) where T : Component
		{
			ICollection<RaycastHit2D> t = CastAll(origin, directionOverride, distanceOverride, layerMaskOverride);
			if (!t.Any(r => r.collider.gameObject.GetComponent<T>() != null)) return null;
			else return (ICollection<T>) t.Select(r => r.collider.gameObject.GetComponent<T>());	
		}


		protected ICollection<RaycastHit2D> CastAll (Vector2 origin, Vector2 directionOverride, float distanceOverride, LayerMask layerMaskOverride)
		{
			if (previousHits == null || previousHits.Length != maxCandidate) previousHits = new RaycastHit2D[maxCandidate];

			if (filter.useDepth)
				raycastCount = Physics2D.RaycastNonAlloc(
					origin + offset,
					directionOverride.normalized,
					previousHits,
					distanceOverride,
					layerMaskOverride.value,
					filter.minDepth,
					filter.maxDepth);
			else raycastCount = Physics2D.RaycastNonAlloc(
					origin + offset,
					directionOverride.normalized,
					previousHits,
					distanceOverride,
					layerMaskOverride.value);

			if (raycastCount == 0) return null;

			

			//Iterate through the blacklists.
			cache.Clear();
			cache.AddRange(previousHits.Take(raycastCount));

			cache.RemoveAll(e => e.collider == null
							|| (!filter.useTriggers)? e.collider.isTrigger : false
							|| (goFilter != null)? !goFilter.Match(e.transform.gameObject) : false);
			if (cache.Count == 0) return null;
			else return cache;
			// cache.RemoveAll(e => e.collider == null);
			// if (!filter.useTriggers) cache.RemoveAll(e => e.collider.isTrigger);
			// if (goFilter != null) cache.RemoveAll(e => !goFilter.Match(e.transform.gameObject));
			// if (cache.Count == 0) return null;
			// else return cache;
		}

		public RaycastHit2D Cast (Vector2 origin)
		{
			return Cast(origin, castDistance);
		}
		public RaycastHit2D Cast (Vector2 origin, float distanceOverride)
		{
			return Cast(origin, DirectionValue, distanceOverride);
		}

		public RaycastHit2D Cast (Vector2 origin, Vector2 directionOverride, float distanceOverride)
		{
			return Cast(origin, directionOverride, distanceOverride, (filter.useLayerMask)? filter.layerMask : (LayerMask) System.Int32.MaxValue);
		}

		public RaycastHit2D Cast (Vector2 origin, Vector2 directionOverride, float distanceOverride, LayerMask layerMaskOverride)
		{
			if (goFilter != null)
			{
				ICollection<RaycastHit2D> r = CastAll(origin, directionOverride, distanceOverride, layerMaskOverride);
				if (r == null || r.Count == 0)
				{
					previousHits[0] = new RaycastHit2D();
				}
				else
				{
					previousHits[0] = r.First();
				}
				return previousHits[0];
			}
			else
			{
				if (filter.useDepth)
					previousHits[0] = Physics2D.Raycast(
						origin + offset,
						directionOverride.normalized,
						distanceOverride,
						layerMaskOverride.value,
						filter.minDepth,
						filter.maxDepth);
				else previousHits[0] = Physics2D.Raycast(
						origin + offset,
						directionOverride.normalized,
						distanceOverride,
						layerMaskOverride.value);

				return previousHits[0];
			}
		}

		public void Draw (Vector2 origin)
		{
			Debug.DrawRay(origin + offset, DirectionValue * castDistance, Color.red);
		}

	}

	
}
